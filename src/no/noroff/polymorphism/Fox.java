package no.noroff.polymorphism;

public class Fox extends Animal{

    public void makeNoise()
    {
        System.out.println("A terrible noise");
    }
}
