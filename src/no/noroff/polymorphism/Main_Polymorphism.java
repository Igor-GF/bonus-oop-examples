package no.noroff.polymorphism;

import java.util.ArrayList;

public class Main_Polymorphism
{
    public static void main(String[] args)
    {
        System.out.println("--- POLYMORPHISM ---\n");

        // I can have a pet of type cat
        Animal pet = new Cat();
        System.out.println(pet.getInfo());

        // pets can make noise ...
        pet.makeNoise();

        // Or we cast pet back to cat first
        ((Cat)pet).makeNoise();

        System.out.println("\n--- PET COLLECTION ---\n");

        // A collection of animals
        ArrayList<Animal> pets = new ArrayList<Animal>();

        // Two cats and one mouse
        pets.add(new Cat());
        pets.add(new Cat());
        pets.add(new Mouse());

        for (Animal animal : pets) {   // Get each animal
            animal.makeNoise();        // Ask it to makeNoise()
        }

        System.out.println("\n--- --- ---\n");

        // This example would have worked with the inheritance model using Person/Teacher/Student too


    }




}
