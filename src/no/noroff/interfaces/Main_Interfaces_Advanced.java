package no.noroff.interfaces;

import java.util.ArrayList;

public class Main_Interfaces_Advanced
{
    public static void main(String[] args)
    {
        System.out.println("--- INTERFACES ADVANCED ---\n");
        ArrayList<Steerable> steerables = new ArrayList<>();
        steerables.add(new Trolley());
        steerables.add(new Car());

        for (Steerable thing: steerables)
        {
            thing.steer();
            thing.stop();

            // Instanceof works with both classes and interfaces
            if(thing instanceof Refuelable)
            {
                ((Refuelable) thing).refuel();
            }
        }
        System.out.println("\n--- --- ---\n");
    }
}
