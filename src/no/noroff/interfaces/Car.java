package no.noroff.interfaces;

public class Car implements Steerable, Refuelable
{
    public void steer()
    {
        System.out.println("A car is steered a steering wheel.");
    };
    public void stop()
    {
        System.out.println("A car stops when you use the brake.");
    };

    public void refuel()
    {
        System.out.println("A car is refueled at the gas station.");
    }

    public String toString() {
        return "This is a car.";
    }
}
