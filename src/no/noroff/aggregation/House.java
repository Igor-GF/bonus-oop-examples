package no.noroff.aggregation;

public class House
{
    Mailbox mailbox;

    public House(Mailbox mailbox)
    {
    this.mailbox = mailbox;
    }

    @Override
    public String toString()
    {
        return "A house with a mailbox. (Aggregation)";
    }

    public String getAddress()
    {
        return mailbox.getAddress();
    }
}
