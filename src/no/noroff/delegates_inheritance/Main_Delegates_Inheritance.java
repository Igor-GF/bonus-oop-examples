package no.noroff.delegates_inheritance;

import javax.swing.*;

public class Main_Delegates_Inheritance
{

    // As Java does not have first-class functions, we realise delegates through the use of interfaces/inheritance and polymorphism

    // This example uses inheritance

    public static void main(String[] args)
    {
        System.out.println("--- DELEGATES VIA INHERITANCE ---\n");

        // Pay by cash
        Cash cash = new Cash(100.0f, "kroner");
        cash.pay();

        // Pay by card
        Card card = new Card(42.0f, "krona");
        card.pay();

        System.out.println("\n--- --- ---\n");

        Teller Joe_Wallmart = new Teller();

        Joe_Wallmart.handlePayment(new Cash(11.99f, "dollars"));

        System.out.println("\n--- --- ---\n");
    }
}
