package no.noroff.delegates_interfaces;

public class Main_Delegates_Interfaces
{
    public static void main(String[] args)
    {
        System.out.println("--- DELEGATES VIA INTERFACES ---\n");

        // Pay by cash
        Cash cash = new Cash(999.99f, "euro");
        cash.pay();

        // Pay by card
        Card card = new Card(0.98f, "pound sterling");
        card.pay();

        System.out.println("\n--- --- ---\n");

        Teller Joe_Wallmart = new Teller();

        Joe_Wallmart.handlePayment(new Card(3.50f, "dollars"));

        System.out.println("\n--- --- ---\n");

        Joe_Wallmart.handlePayment(new MagicVoucher());

        System.out.println("\n--- --- ---\n");

    }
}
