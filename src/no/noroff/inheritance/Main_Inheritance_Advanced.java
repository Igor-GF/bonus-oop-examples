package no.noroff.inheritance;

import no.noroff.polymorphism.Animal;

import java.util.ArrayList;

public class Main_Inheritance_Advanced
{
    public static void main(String[] args)
    {
        System.out.println("--- A COLLECTION OF PEOPLE ---\n");

        ArrayList<Person> people = new ArrayList<>(); // You may leave out the type between the second <>

        people.add(new Person("Max", "Mustermann"));
        people.add(new Teacher("Craig", "Marais", "Accelerate", 100));
        people.add(new Student("Greg", "Linklater", "Life"));

        for (Person person : people)
        {
            System.out.println("--- --- ---");
            System.out.println(person);

            if(person instanceof Teacher){
                System.out.println("This person is a teacher.");
                System.out.println("Department: " + ((Teacher)person).getDepartment());
            }

            if(person instanceof Student){
                System.out.println("This person is a student.");
                System.out.println("Course: " + ((Student)person).getCourse());
            }
            System.out.println("--- --- ---\n");
        }

        System.out.println("\n--- --- ---\n");
    }
}
