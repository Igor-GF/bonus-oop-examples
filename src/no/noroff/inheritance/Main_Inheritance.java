package no.noroff.inheritance;

public class Main_Inheritance
{
    public static void main(String[] args)
    {
        System.out.println("--- INHERITANCE ---\n");

        // Consider a person, Max Mustermann
        Person person_1 = new Person("Max", "Mustermann");
        System.out.println("This is the first person: " + person_1);

        // ┌────────┐
        // │ Person │
        // └────────┘

        System.out.println("\n--- --- ---\n");

        // Now consider a teacher, Craig Marais
        Teacher person_2 = new Teacher("Craig", "Marais", "Accelerate", 100);
        System.out.println("This is the second person: " + person_2);
        System.out.println("This person works in: " + person_2.getDepartment());

        // ┌────────┐
        // │ Person │
        // └────────┘
        //      ^       (This arrow should be touching the box, but I cannot draw that here properly)
        //      │
        //      │
        // ┌────┴────┐
        // │ Teacher │
        // └─────────┘
        // A Teacher is a "type" of person

        System.out.println("\n--- --- ---\n");

        // Now consider a student, Greg Linklater
        Student person_3 = new Student("Greg", "Linklater", "Life");
        System.out.println("This is the third person: " + person_3);
        System.out.println("This person is a student of: " + person_3.getCourse());

        // ┌────────┐
        // │ Person │
        // └────────┘
        //      ^       (This arrow should be touching the box, but I cannot draw that here properly)
        //      │
        //      ├──────────────┐
        // ┌────┴────┐    ┌────┴────┐
        // │ Teacher │    │ Student │
        // └─────────┘    └─────────┘
        // Teacher and Student are both "types" of person

        System.out.println("\n--- --- ---\n");
    }
}
