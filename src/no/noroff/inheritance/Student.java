package no.noroff.inheritance;

public class Student extends Person
{
    private String course;

    public Student(String firstName, String lastName, String course) {
        super(firstName, lastName);

        this.course = course;
    }

    public String getCourse()
    {
        return course;
    }
}
