package no.noroff.association;

public class Mailbox
{
    private String address;

    public Mailbox(String address){
        this.address = address;
    }
    @Override
    public String toString() {
        return "A simple mailbox.";
    }

    public String getAddress(){
        return address;
    }
}
