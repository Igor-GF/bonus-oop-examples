package no.noroff.association;

public class House
{
    Mailbox mailbox;

    public House()
    {

    }

    public void setMailbox(Mailbox mailbox)
    {
        this.mailbox = mailbox;
    }

    @Override
    public String toString()
    {
        return "A house that could be associated with a mailbox.";
    }

    public String getHouseAddress()
    {
        if(mailbox != null)
        {
            return mailbox.getAddress();
        }
        else
        {
            return "No address found (no mailbox)";
        }
    }

}
